const async = require('async')

console.log('Series start');

function A (arg) {
  setTimeout(() => {
    console.log('1')
  }, 3000);
}
function B (arg) {
  setTimeout(() => {
    console.log('2')
  }, 2000);
}
function C (arg) {
  console.log('3')
}


async.waterfall([
  function (callback) {
    A(1);
    callback(null);
  },
  function (callback) {
    B(2);
    callback(null);
  },
  function (callback) {
    C(3);
    callback (null);
  }],
  function (err, results) {
    if (err) {
      console.log("err[" + err + "]");
    }
    console.log("--series finished");
  }
)