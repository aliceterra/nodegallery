const async = require('async')

console.log('Series start');

async.series([
  function (callback) {
    console.log("1");
    setTimeout(function() {
      console.log("1.done");
      callback(null,1);
    },500);
  },
  function (callback) { 
    console.log("2");
    setTimeout(() => {
      console.log("2.done");
    }, 500);
  },
  function (callback) {
    console.log("3");
    setTimeout(() => {
      console.log("2.done");
    }, 500);
  }, function  (err, results) {
    if(err) { 
      console.log("err[" + err + "]");
    }
    console.log("--series finished");
  }
])