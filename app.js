"use strict"
const fs = require('fs');
const exif = require('jpeg-exif');
const ejs = require('ejs');

const dir = './images/';
const jpg = '.jpg';
const view = './views/archivePage.ejs';
const destView = './dest.html'
const testImages = ["1.jpg",'2.jpg','3.jpg','1.jpg'];
let jpgList = [];

getJpgList()
setTimeout(() => {
  console.log(jpgList)
  renderArchivePhotoPage(jpgList)

}, 1000);


function getJpgList (dir, callback) {

  fs.readdir('./images', function (err, files) {
      if (err){console.log(err)}
      else{
        for (const key in files) {
          if (files[key].match(jpg)) {
            jpgList.unshift(files[key])
          }
        }
      } 
      ;
      
    });
  }

function renderArchivePhotoPage (files){
  //console.log(files)
  const numImage = files.length;
  //console.log(numImage)

  const json = {};
  json.images = [];
  //JSON生成
  for (let key = 0; key < numImage; key++) {
    

    exif.parse(dir+files[key], function (err, data) {
      if(err) {console.log(err)}
      else {
        json.images[key] = {};
        //console.log(data.SubExif.DateTimeOriginal)//ファイル確認用
        json.images[key].title= files[key]
        json.images[key].url = dir+files[key]
        json.images[key].caption = data.SubExif.DateTimeOriginal
      }
      // json から空要素を削除した配列arrを作成してlengthを比較
      // 全ての要素が揃ったらdestViewで
      const arr = json.images.filter(v =>v)


      if(arr.length === numImage){
        //console.log(json)
        ejs.renderFile(view, {
          parameter: json.images
        }, function (err, html) {
          if (err) {
            console.log(err)
          } else {
            console.log(html);
            fs.writeFile(destView, html, 'utf8', (err) => {
              if (err) {
                console.log(err)
              } else {
                console.log(':file saved!')
              }
            })
          }
          }
        );
      }
    })
  }
}