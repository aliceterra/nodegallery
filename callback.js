const fs = require('fs');

const word = 'C'


function f1(callback){
      console.log('f1');
      fs.readdir('./images', function (err, files) {
        if (err) throw err;
        console.log(files);
      });
      callback('a')
    }

    function f2(dir) {
      console.log('f2'+dir);
    }

    f1(f2);

    //=> f1
    //=> f2

    function fileList(callback) {
      fs.readdir('./images', function (err, files) {
        if (err) throw err;
        console.log(files);
      });
      callback(null,1)
    }

    fileList(f2)