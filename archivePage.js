"use strict"

const fs = require('fs');
const exif = require('jpeg-exif');
const ejs = require('ejs');

const dir = './images/';
const jpg = '.jpg';
const view = './views/singlePage.ejs';
const destView = './dest.html'
const testImage = "./images/1.jpg"
let json = {};

exif.parse(testImage, (err, data) => {
  const imageJson = {};
  imageJson.title = data.Make;
  imageJson.url = testImage;
  imageJson.caption = data.SubExif.LensModel;
  //console.log(imageJson)

  ejs.renderFile(view, {
    parameter: imageJson
  }, function (err, html) {
    if (err) {
      console.log(err)
    } else {
      console.log(html);
      fs.writeFile(destView, html, 'utf8', (err) => {
        if (err) {
          console.log(err)
        } else(console.log('file saved'))
      })
    }
  })
})
