const fs = require('fs');

// Promiseを返す関数
const readFilePromise = (filePath) => {
    return new Promise((resolve) => {
        fs.readFile(path = filePath, options = {
            encoding: 'utf8'
        }, (err, data) => {
            resolve(data);
        });
    });
}

// 非同期でテキストファイルを読み込む
readFilePromise('foo.txt')
    .then((data) => { // 非同期処理が完了するたびにthen()メソッドの中に書いた関数が実行される
        setTimeout(() => {
        console.log('foo.txtを読み込みました', data);
        return readFilePromise('bar.txt');
        }, 1000);
      
    })
    .then((data) => {
        console.log('bar.txtを読み込みました', data);
        return readFilePromise('buz.txt');
    })
    .then((data) => {
        console.log('buz.txtを読み込みました', data);
    });