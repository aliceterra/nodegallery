"use strict"
const fs = require('fs');
const exif = require('jpeg-exif');
const ejs = require('ejs');

const dir = './images/';
const jpg = '.jpg';
const view = './views/singlePage.ejs';
const destView = './dest.html'
const testImages = ["1.jpg", "2.jpg"];

for (const key in testImages) {
  
  renderSinglePhotoPage (testImages[key])
}


function renderSinglePhotoPage (file){
  const fileName = file.split(jpg)[0]
  console.log("input:"+file)
  exif.parse(dir+file, function(err, data)  {
    if(err){console.log(err)}
    else {
    const imageJson = {};
    imageJson.title = data.Make;
    imageJson.url = dir+file
    imageJson.caption = data.SubExif.LensModel;
    //console.log(imageJson)

    ejs.renderFile(view, {
      parameter: imageJson
    }, function (err, html) {
      if (err) {
        console.log(err)
      } else {
        //console.log(html);
        fs.writeFile(fileName+ ".html" , html, 'utf8', (err) => {
          if (err) {
            console.log(err)
          } else{
            console.log(fileName+':file saved!')}
        })
      }
    })
  }
})
}