const async = require('async')

console.log('Series start');

async.waterfall([
  function (callback) {
    console.log("1");
    setTimeout(function () {
      console.log("1.done");
      callback(null, 1);
    }, 500);
  },
  function (arg, callback) {
    console.log("2:" + arg);
    setTimeout(() => {
      console.log("2.done");
      callback(null, 1);

    }, 500);
  },
  function (err, results) {
    if (err) {
      console.log("err[" + err + "]");
    }
    console.log("--series finished");
  }
])