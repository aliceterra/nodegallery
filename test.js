"use strict"

const fs = require('fs');
const exif = require('jpeg-exif');
const ejs = require('ejs');

const dir = './images/';
const jpg = '.jpg';
const view = './views/gallery.ejs';
let json = {};

fs.readdir(dir, function (err, files) {
  if (err) throw err;
  //console.log(files);
  for (const key in files) {
    if(!files[key].match(jpg)){delete files[key]}
    else {files[key] = dir + files[key]}
  }
  buildJson(files);
//  return files;
});
//受け取った配列からejsに渡すJSONを生成する
//必要な変数はtitle,url,caption,dateTime
function buildJson (arr) {
  //console.log(arr)
  for (const key in arr) {
    if(arr[key]){
      exif.parse(arr[key], (err, data) => {
        if(err) {console.log('Exif parse error')}
        //EJS用のJSONを生成する
        else {
          json[key] = {};
          json[key].title = data.Make;
          json[key].url = arr[key];
          json[key].caption = data.SubExif.LensModel;
        }
        //JSONの確認用
    //   console.log(json)
    // console.log(json[0])
      
      ejs.renderFile(view, {
        json: json[2],
      }, function (err, html) {
        //console.log(html)
      });
      
    })

    
    }
  }

}
